/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2017 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */
/*============================================================================
Name:          	main.c
Author:        	Per �stberg
Compile with:  	Atollic TrueStudio 7.0.1
Date:          	2017-02-19
Description:   	Lightlogger - an ADC is read and its values presented on
				KEY&LED and OLED. For the demo a photoresistor
				is connected to pin PC2
============================================================================*/

#include "font.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/


#define SCL(x)			HAL_GPIO_WritePin(SCL_GPIO_Port, SCL_Pin, x)
#define SDA(x)			HAL_GPIO_WritePin(SDA_GPIO_Port, SDA_Pin, x)
#define rSDA			HAL_GPIO_ReadPin(SDA_GPIO_Port, SDA_Pin)

#define STARTBIT		SDA(LOW);  CDELAY	//SCK=HIGH
#define STOPBIT			SCL(HIGH); CDELAY; SDA(HIGH); CDELAY; CDELAY;	//SCK=HIGH

#define ADDRESS			(0x3c<<1)
#define WRITE			0
#define READ			1
#define HORIZONTAL		0
#define VERTICAL		1
#define COMMAND			0x00
#define DATA			0x40

#define CONTRAST		0x81	//A
#define DISPLAYON(x)	(0xa4+x)
#define INVERSE(x)		(0xa6+x)
#define ONOFF(x)		(0xae +x)

#define LOCOL(x)		(x)
#define HICOL(x)		(0x10+x)
#define ADDRESSING		0x20	//A
#define SETCOL			0x21	//AB
#define SETPAGE			0x22	//AB
#define SETPAGESTART(x)	(0xb0+x)

#define STARTLINE(x)	(0x40+x)
#define REMAP(x)		(0xa0+x)
#define MULTIPLEX		0xa8	//A
#define OUTDIR(x)		(0xc0+(x<<3))
#define OFFSET			0xd3	//A
#define COMPINS			0xda	//A

#define DIVIDEFREQ		0xd5	//A
#define PRECHARGE		0xd9	//A
#define VCOMH			0xdb	//A
#define NOP				0xe3

#define CHARGEPUMP		0x8d	//A

#define DISABLESCROLL	0x2e

#define STB(x)		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, x)			//A3
#define CLK(x)		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, x)			//A4
#define DIO(x)		HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, x)			//A5
#define rDIO		HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0)				//A5

#define LD2(x)		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, x)	//LD2
#define HIGH		GPIO_PIN_SET
#define LOW			GPIO_PIN_RESET
#define CDELAY		cdelay();

#define DATA		0x40
#define TESTMODE	0x08
#define FIXED		0x04
#define READ_SPI		0x02

#define DISPLAY		0x80	//+ intensity
#define	ON			0x08

#define ADDRESS_SPI		0xc0	//+ address


// Stating the obvious to avoid magic numbers in code
#define NO_OF_BYTES_IN_DWORD	4
#define NO_OF_BITS_IN_BYTE		8
#define NO_OF_BYTES_IN_GLYPH	8

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Numeric representations used for the 7-segment (SEG7) display
 * (numbers indicate the position [1-8] in the byte, LSB
 * in the rightmost position)
 *
 *	   1
 * 	  ---
 * 6 |   | 2
 *    ---     - > 7
 * 5 |   | 3
 *    ---
 *     4
 *		  .	  - > 8
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#define SEG7_ZERO 	0x3f	//	00111111
#define SEG7_ONE 	0x06	//  00000110
#define SEG7_TWO 	0x5B	//	01011011
#define SEG7_THREE 	0x4F	//	01001111
#define SEG7_FOUR 	0x66	//	01100110
#define SEG7_FIVE 	0x6D	//	01101101
#define SEG7_SIX 	0x7D	//	01111101
#define SEG7_SEVEN 	0x07	//	00000111
#define SEG7_EIGHT 	0x7F	//	01111111
#define SEG7_NINE 	0x6F	//	01101111
#define SEG7_A 		0x77	//	01110111
#define SEG7_B 		0x7C	//	01111100
#define SEG7_C 		0x39	//	00111001
#define SEG7_D 		0x5E	//	01011110
#define SEG7_E 		0x79	//	01111001
#define SEG7_F  	0x71	//	01110001

// Enum used to select output mode HEX/DEC
typedef enum
{
	OUTPUT_MODE_DECIMAL = 0,
	OUTPUT_MODE_HEX
}	OutputMode7Seg;

#define SCREEN_WIDTH			128
#define SCREEN_HEIGHT			64
#define NO_OF_PAGES				8

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void sendbyte_SPI(uint8_t data);
void cdelay(void);

uint8_t decode(uint32_t value);

void wrapandsendbyte_SPI(uint8_t data);
uint32_t readbytes_SPI(void);
void output_to_card(uint8_t);
uint8_t convertbytefordisplay(uint8_t data, uint8_t pos, OutputMode7Seg mode);
uint8_t reversebits(uint8_t);
void put_OLED_char(volatile char p);
void printstr(char *string);
GPIO_PinState sendbyte_I2C(uint8_t data);
void init_peripherals_SPI(void);
void init_peripherals_I2C(void);
void clearscreen(void);
void redraw();
void OLED_pixel(uint8_t buffer[], uint32_t x,uint32_t y);

void print_bar(uint32_t x,uint32_t y);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

// Vertical line drawing look up table for first byte
static const uint8_t LUT_FB[] = { 0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE };

static const uint8_t LUT_HEX[] = { 0x3f, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71 };

// Image bytes
uint8_t OLED [SCREEN_WIDTH * NO_OF_PAGES];

int main(void)
{

	uint32_t light_observations[SCREEN_WIDTH] = {0};
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_ADC1_Init();

	/* USER CODE BEGIN 2 */
	init_peripherals_SPI();
	init_peripherals_I2C();

	uint32_t ADCValue = 0;
	clearscreen();

	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		if (HAL_ADC_Start(&hadc1) != HAL_OK)
		{
			/* Start Conversation Error */
			// Error_Handler();
		}
		if (HAL_ADC_PollForConversion(&hadc1, 500) != HAL_OK)
		{
			/* End Of Conversion flag not set on time */
			// Error_Handler();
			ADCValue=-1;
		}
		else
		{
			/* ADC conversion completed */
			/*##-5- Get the converted value of regular channel ########################*/
			ADCValue = HAL_ADC_GetValue(&hadc1);
		}
		HAL_ADC_Stop(&hadc1);

		// Out to KEY & LED
		output_to_card(ADCValue);

		// Shift the stored values
		for (uint32_t var = 0; var < SCREEN_WIDTH - 1; ++var)
			light_observations[var] = light_observations[var+1];

		// Insert the latest reading in the last slot, shift to 6 bits so it fits in the 64 px column
		light_observations[SCREEN_WIDTH - 1] = ADCValue>>2;

		// Refresh the image
		for (int x = 0; x < SCREEN_WIDTH; ++x)
			print_bar(x, light_observations[x]);

		// redraw
		redraw();

		// wait
		HAL_Delay(200);
	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 100;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
	{
		Error_Handler();
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

	ADC_ChannelConfTypeDef sConfig;

	/**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	 */
	hadc1.Instance = ADC1;
	hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	hadc1.Init.Resolution = ADC_RESOLUTION_8B;
	hadc1.Init.ScanConvMode = DISABLE;
	hadc1.Init.ContinuousConvMode = DISABLE;
	hadc1.Init.DiscontinuousConvMode = DISABLE;
	hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion = 1;
	hadc1.Init.DMAContinuousRequests = DISABLE;
	hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	if (HAL_ADC_Init(&hadc1) != HAL_OK)
	{
		Error_Handler();
	}

	/**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_12;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
	{
		Error_Handler();
	}

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		Error_Handler();
	}

}

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : DIO_Pin CLK_Pin */
	GPIO_InitStruct.Pin = DIO_Pin|CLK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pin : LD2_Pin */
	GPIO_InitStruct.Pin = LD2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : STB_Pin SDA_Pin */
	GPIO_InitStruct.Pin = STB_Pin|SDA_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/*Configure GPIO pin : SCL_Pin */
	GPIO_InitStruct.Pin = SCL_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SCL_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOC, DIO_Pin|CLK_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, LD2_Pin|SCL_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOB, STB_Pin|SDA_Pin, GPIO_PIN_RESET);

}

/* USER CODE BEGIN 4 */

/**
 * @brief  Function used for SPI communication signalling
 * cdelay: wait approx. 1 us
 * @param  None
 * @retval None
 */
void cdelay(void)
{
	volatile int delay;
	for (delay = 8; delay != 0; delay--);
}


/**
 * @brief  Function used to output one byte on the SPI bus
 * Requires that a transaction is started (STB = LOW)
 * @param  uint8_t data to transmit
 * @retval None
 */
void sendbyte_SPI(uint8_t data)
{
	uint8_t i;

	for(i = 0x1; i != 0; i <<= 1)	// MASK for each bit in data 1..7
	{
		DIO(!!(data & i));	// OUT new data to BUS
		CLK(LOW);			// Alert: new data on negative edge
		CDELAY;				// Allow signal to stabilise
		CLK(HIGH);			// Slave read on positive edge
		CDELAY;				// Allow signal to stabilise
	}
}

/**
 * @brief  Wraps a byte in a SPI transaction sequence by initializing
 * transaction (STB = LOW), sending data, and ending transaction (STB = HIGH)
 * @param  uint8_t data to transmit
 * @retval None
 */
void wrapandsendbyte_SPI(uint8_t data)
{
	STB(LOW);				// init transaction
	CDELAY;					// Delay before sending data
	sendbyte_SPI(data);		// Output the byte on the bus
	CDELAY;					// Delay before closing the data-sending sequence
	STB(HIGH);				// end transaction
	CDELAY;					// Delay to ensure command is closed properly
}

/**
 * @brief  Read 4 bytes from SPI slave
 * @param  none
 * @retval uint32_t 4 bytes read from SPI slave
 */
uint32_t readbytes_SPI(void)
{
	int bitcount, bytecount;
	uint32_t return_value = 0;

	STB(LOW);				// Start transaction
	CDELAY;					// Delay before issuing command
	sendbyte_SPI(DATA | READ_SPI);	// Issue Command: Data - Read

	DIO(HIGH); 				// Make sure DIO -> HIGH to be able to read data!
	CDELAY;					// Allow signal to stabilise

	// Reading 4 bytes
	for(bytecount = 0; bytecount < NO_OF_BYTES_IN_DWORD; bytecount++)
	{
		for(bitcount = 0; bitcount < NO_OF_BITS_IN_BYTE; bitcount++)
		{

			CLK(LOW);		// Slave to output data on bus
			CDELAY;			// Allow signal to stabilise

			CLK(HIGH);		// Master to read data

			// Collect the bits in a 32 bit int
			return_value |= (rDIO << (bitcount + (bytecount * NO_OF_BITS_IN_BYTE)));

			CDELAY;			// Wait 1/2 clockcycle

		}

		CDELAY;				// Delay after each byte on HIGH clock
	}

	CDELAY;					// Delay after reading sequence before ending transaction
	STB(HIGH);				// End transaction
	CDELAY;					// Delay to ensure command is closed properly
	return return_value;

}

/**
 * @brief  Reverse the order of the bits in a byte
 * http://www.geeksforgeeks.org/write-an-efficient-c-program-to-reverse-bits-of-a-number/
 *
 * @param  uint8_t num byte to reverse
 * @retval uint8_t byte reversed
 */
uint8_t reversebits(uint8_t num)
{
	uint8_t reverse_num = 0;
	int i;
	for (i = 0; i < NO_OF_BITS_IN_BYTE; i++)
	{
		// if bit@i is set in input, set corresponding 'mirror' bit in output
		if((num & (1 << i)))
			reverse_num |= 1 << ((NO_OF_BITS_IN_BYTE - 1) - i);
	}
	return reverse_num;
}

/**
 * @brief  Output a number to the LEDs and 7 Segment display
 *
 * @param  uint8_t data number to write to LEDs and 7SEG display
 * @retval none
 */
void output_to_card(uint8_t data)
{

	STB(LOW);					// START sending transaction
	CDELAY;						// Delay before issuing command
	sendbyte_SPI(ADDRESS_SPI);	// Issue Command: Address - Start @ 0x00

	/*
		Since data read is MSB -> LSB and we want to
		output MSB on LED 8 - reverse the bits
	 */
	uint8_t reversed_data = reversebits(data);
	uint8_t i, j;

	// MASK for each bit in data 1..7
	for(i = 0x1, j = 1; i != 0; i <<= 1, j++)
	{
		// OUTPUT: -> 7-seg on even address
		sendbyte_SPI(convertbytefordisplay(data, j, OUTPUT_MODE_DECIMAL));

		// OUTPUT: -> LED on uneven address
		sendbyte_SPI(!!(reversed_data & i));
	}

	STB(HIGH);					// END sending transaction
	CDELAY;						// Delay to ensure command is closed properly
}


/**
 * @brief  The function calculates the byte to display on the
 * 7 Segment display for a given position and diaplay mode
 *
 * @param  uint8_t data the number to convert
 * @param  uint8_t pos the 7-SEG position
 * @param  OutputMode7Seg mode Output in Hexadecimal or Decimal
 *
 * @retval uint8_t byte to display
 */
uint8_t convertbytefordisplay(uint8_t data, uint8_t pos, OutputMode7Seg mode)
{
	uint8_t tmp = 0;
	uint8_t return_value = 0;

	// Only the positions 6, 7 and 8 are used to display an 8 bit number
	// 2 positions for HEX and 3 for DEC. Other positions are turned off.
	switch (pos) {
	case 1:
		return_value = 0;
		break;
	case 2:
		return_value = 0;
		break;
	case 3:
		return_value = 0;
		break;
	case 4:
		return_value = 0;
		break;
	case 5:
		return_value = 0;
		break;
	case 6:
		// 3rd position used only for DEC 100's.
		if(mode == OUTPUT_MODE_DECIMAL && data > 99)
		{
			tmp = data / 100;
			return_value = LUT_HEX[tmp];
		} else
		{
			return_value = 0;
		}
		break;
	case 7:
		// 2nd position used for DEC and HEX 10's.
		if(mode == OUTPUT_MODE_HEX && data > 0xf)
		{
			tmp = data / 0x10;
			return_value = LUT_HEX[tmp];
		} else if(mode == OUTPUT_MODE_DECIMAL)
		{
			if(data > 99)
			{
				tmp = data % 100;
				tmp = tmp / 10;

				return_value = LUT_HEX[tmp];
			} else if(data > 9)
			{
				tmp = data / 10;
				return_value = LUT_HEX[tmp];
			} else
			{
				return_value = 0;
			}
		} else
		{
			return_value = 0;
		}
		break;
	case 8:
		// 1st position used for DEC and HEX 1's.
		if(mode == OUTPUT_MODE_HEX)
		{
			tmp = data % 0x10;
			return_value = LUT_HEX[tmp];
		} else if(mode == OUTPUT_MODE_DECIMAL)
		{
			tmp = data % 10;
			return_value = LUT_HEX[tmp];
		} else
		{
			return_value = 0;
		}
		break;
	default:
		return_value = 0;
		break;
	}
	return return_value;
}

/**
 * @brief  The function converts the 32 bits read from LED&KEY
 * to one byte in the order MSB -> LSB as specified in the datasheet
 *
 * @param  uint32_t value the 32 bits to convert
 * @retval uint8_t converted byte
 */
uint8_t decode(uint32_t value)
{
	return
			(value & 0x00000001) << 7  |
			(value & 0x00000100) >> 2  |
			(value & 0x00010000) >> 11 |
			(value & 0x01000000) >> 20 |
			(value & 0x00000010) >> 1  |
			(value & 0x00001000) >> 10 |
			(value & 0x00100000) >> 19 |
			(value & 0x10000000) >> 28;
}

void init_peripherals_I2C(void)
{

	STOPBIT;						// Idle: CLOCK and DATA lines high

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * 						Transaction 1: Configuration 				 *
	 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	STARTBIT;						// Start transaction by pulling DATA low and waiting 1 us

	LD2(sendbyte_I2C(ADDRESS+WRITE));	// Alert slave by sending ADDRESS (OLED display) and operation (WRITE)

	LD2(sendbyte_I2C(COMMAND));			// We want to send commands to configure the OLED

	// The rest of this transaction is initialization of OLED
	LD2(sendbyte_I2C(ONOFF(0)));
	LD2(sendbyte_I2C(DIVIDEFREQ));	LD2(sendbyte_I2C(0x80));
	LD2(sendbyte_I2C(MULTIPLEX));	LD2(sendbyte_I2C(63));
	LD2(sendbyte_I2C(OFFSET));		LD2(sendbyte_I2C(0));
	LD2(sendbyte_I2C(STARTLINE(0)));
	LD2(sendbyte_I2C(CHARGEPUMP));	LD2(sendbyte_I2C(0x14));
	LD2(sendbyte_I2C(ADDRESSING));	LD2(sendbyte_I2C(HORIZONTAL));
	LD2(sendbyte_I2C(REMAP(1)));
	LD2(sendbyte_I2C(OUTDIR(1)));
	LD2(sendbyte_I2C(COMPINS));		LD2(sendbyte_I2C(0x12));
	LD2(sendbyte_I2C(CONTRAST));	LD2(sendbyte_I2C(0xcf));
	LD2(sendbyte_I2C(PRECHARGE));	LD2(sendbyte_I2C(0xf1));
	LD2(sendbyte_I2C(VCOMH));		LD2(sendbyte_I2C(0x40));
	LD2(sendbyte_I2C(DISPLAYON(0)));
	LD2(sendbyte_I2C(INVERSE(0)));
	LD2(sendbyte_I2C(DISABLESCROLL));
	LD2(sendbyte_I2C(ONOFF(1)));
	LD2(sendbyte_I2C(SETCOL));		LD2(sendbyte_I2C(0));	LD2(sendbyte_I2C(127));
	LD2(sendbyte_I2C(SETPAGE));		LD2(sendbyte_I2C(0));	LD2(sendbyte_I2C(7));
	STOPBIT;						// End transaction 1 by pulling dataline high when clock is HIGH


}

/**
 * @brief  	This function clears the OLED by writing 0x0 to
 * all pixels
 *
 * @param  None
 * @retval None
 */

void clearscreen(void)
{

	STARTBIT;						// Start a transaction
	LD2(sendbyte_I2C(ADDRESS+WRITE));	// Alert slave by sending its ADDRESS and what we want (WRITE)
	LD2(sendbyte_I2C(DATA));			// Set up a data transfer

	// Write to all pixels
	for (int i = 0; i < SCREEN_WIDTH * NO_OF_PAGES; ++i)
		LD2(sendbyte_I2C(0x00));

	STOPBIT;						// End transaction
}

/**
 * @brief  	This function sends the OLED array to the display over I2C
 *
 * @param  None
 * @retval None
 */

void redraw()
{

	STARTBIT;						// Start a transaction
	LD2(sendbyte_I2C(ADDRESS+WRITE));	// Alert slave by sending its ADDRESS and what we want (WRITE)
	LD2(sendbyte_I2C(DATA));			// Set up a data transfer
	// End transaction
	// Write to all pixels
	for (int i = 0; i < SCREEN_WIDTH * NO_OF_PAGES; i++)
		LD2(sendbyte_I2C(OLED[i]));

	STOPBIT;						// End transaction
}

// SPI
void init_peripherals_SPI(void)
{
	STB(HIGH);
	DIO(HIGH);
	CLK(HIGH);

	// Wait 1 us before communication starts
	CDELAY;

	// Issue Command: Display Control - Display ON, Max Brightness
	wrapandsendbyte_SPI(DISPLAY | ON | 0x0F);

	// Issue Command: Data - Automatic addressing
	wrapandsendbyte_SPI(DATA);
}


/**
 * @brief  	This function updates the OLED array with the last ADC value
 *
 *	Function based on code @ https://www.ccsinfo.com/forum/viewtopic.php?t=52861
 *
 * @param  x: column, y ADC value (6 bit)
 * @retval None
 */
void print_bar(uint32_t x,uint32_t y)
{
	if(!x)
		return;
	if(!y)
		return;
	uint8_t tmp = 0;
	uint32_t page;
	uint32_t bit;
	page = (y / 8);			// Which page 0-7?
	bit = y-(page*8);		// which pix in page?

	// The ADC reading is 6 bit (0-63), the interval corresponding to a page 0-7.
	// This 'ADC page' is set to the corresponding byte found in LUT_FB.
	// For each page / byte in the column above 'ADC page' set all pixels OFF
	// For each page / byte in the column above 'ADC page' set all pixels ON
	for (uint8_t var = 8, i = 0; var != 0; --var, i++) {
		if(page + 1 < var)
		{
			tmp = 0x00;
		} else if(page + 1 > var)
		{
			tmp = 0xFF;
		} else {
			tmp = LUT_FB[bit];
		}
		OLED[i*128+x] = tmp;
	}
}
/**
 * @brief  Function used to output one byte on the SPI bus
 * Requires that a transaction is started (STB = LOW)
 *
 * Modelling the wave on
 * https://cdn.sparkfun.com/assets/6/4/7/1/e/51ae0000ce395f645d000000.png
 *
 * CDELAY represents 1/4 clockcycle on the I2C clock. A call to sendbyte
 * spans 10 clockcycles (8 cs for the data + 2 cs overhead)
 *
 * @param  uint8_t data to transmit
 * @retval GPIO_PinState:_ the ACK/NACK bit for the COMMAND/DATA
 */

GPIO_PinState sendbyte_I2C(uint8_t data)
{
	uint8_t i;				// Loopy int
	GPIO_PinState ack;		// Return value

	// Transaction is initialized when CLOCK is pulled low when DATA is low
	SCL(LOW);
	CDELAY;					// Wait 1/4 cs before starting output data

	// MASK for each bit in data 7..1, MSB first
	for(i = 0x80; i != 0; i >>= 1)
	{
		SDA(!!(data & i));	// Put new data on the bus only on low clock
		CDELAY;				// Wait 1/4 cs before pulling the clock high
		SCL(HIGH);			// Clock out the data: SCL HIGH
		CDELAY;				// Slave to read the data
		CDELAY;				// Delay again just cause we like our curves symmetrical
		SCL(LOW);			// Get ready for new data write (or ACK/NACK read, if last bit), SCL LOW
		CDELAY;				// Wait the remaining 1/4 cs
	}

	SCL(HIGH);				// Clock in the ACK/NACK: SCL HIGH

	CDELAY;					// Wait 1/4 cs
	ack = rSDA;				// Read the ACK/NACK that the slave has put on the bus
	CDELAY;					// Wait 1/4 cs

	// Pull the clock low. Leaving the SCL low at the end of byte-transmission (and ACK/NACK) enables
	// another byte/s to be sent in this transaction; closing the transaction is up to the callee.

	SCL(LOW);

	CDELAY;					// Wait 1/2 cs
	CDELAY;

	SDA(HIGH);				// Set the data line SDA HIGH

	CDELAY;					// Wait 1/2 cs
	CDELAY;

	// Reset the data. SDA LOW enables consecutive calls to sendbyte in the
	// same transaction; closing the transaction is up to the callee.
	SDA(LOW);

	CDELAY;					// Wait 1/4 cs
	return ack;
}

/**
 * @brief  	Print a string to OLED display
 *
 * @param  char * String to output (Latin1)
 * @retval None
 */
void printstr(char *string)
{

	STARTBIT;						// Start transacion
	LD2(sendbyte_I2C(ADDRESS+WRITE));	// Alert slave by sending ADDRESS (OLED display) and operation (WRITE)
	LD2(sendbyte_I2C(DATA));			// Set up a data transfer

	while(1) 						// while there are more characters...
	{
		char x = *(string++); 		// ...read char and increment pointer...
		if(!x) 						// [break if string is terminated]
			break;
		put_OLED_char(x);			// .. and Output the character
	}
	STOPBIT;						// End transaction
}

/**
 * @brief  	This function sends the character's bytes
 * 			as dictated by display HORIZONTAL display setting
 *
 * @param  char The character to output
 * @retval None
 */

void put_OLED_char(volatile char p)
{
	// Send the bytes in corresponding interval to OLED
	for (uint8_t i = 0; i < NO_OF_BYTES_IN_GLYPH; ++i)
		LD2(sendbyte_I2C( font[p * NO_OF_BYTES_IN_GLYPH + i]));
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	/* User can add his own implementation to report the HAL error return state */
	while(1)
	{
	}
	/* USER CODE END Error_Handler */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
